<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'alertes_nom' => "Alertes",
	'alertes_slogan' => "Prévenez vos visiteurs !",
	'alertes_description' => "Alertes permet d'autoriser vos visiteurs (authentifiés) à s'abonner à différents types d'objets SPIP et à recevoir une alerte email quand un article de l'un d'eux est publié sur le site.",
);

